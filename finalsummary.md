*git**Git is the way we treat code and without git, there's no software creation.
*Git helps you to easily keep track of any revision you and your team create throughout your software development.


**repository** A repository is a list of files and directories that you use to track using git (code files). 
*It's the big box into which you and your team used to put your code.

- git init - _create a new loca repository_.
- git clone  : _clone the repository on local system_.
- git add . : _adds changes in the working directory to the staging area_.
- git commit : _saves the files from staging area into the local Git repository_.
- git push origin master : _saves the files in local repo onto the remote repo_.
- git pull - _fetch and merge changes on the remote server to your working directory_.

## Git Internals

Git has three main states that your files can reside in: modified, staged, and committed :

- modified means that you have changed the file but have not committed it to your repo yet.
- staged means that you have marked a modified file in its current version to go into your next picture/snapshot.
- committed means that the data is safely stored in your local repo in form of pictures/snapshots.

At one time there are 3/4 different trees of your software code/repository are present.

- workspace : All the changes you make via Editor(s) is done in this tree of repository.
- staging : All the staged files go into this tree of your repository.
- local Repository : All the committed files go to this tree of your repository.
- remote Repository : This is the copy of your Local Repository but is stored in some server on the Internet. All the changes you commit into the Local Repository are not directly reflected into this tree. You need to push your changes to the Remote Repository.

**Git Workflow**

clone your repo
>git cone repository-name

create a new branch

> git checkout master

> git checkout -b your-branch-name

modify files in your working tree.

Just those changes you want to be part of your next engagement are selectively staged.

>git add

the commit you made,which brings the files to your Local Git Repository as they are in the staging area and stores that are permanently snapshot.

>git commit -sv 

You push the files as they are in the Local Git Repository and store them permanently in your Remote Git Repository.

>git push origin branch-name
